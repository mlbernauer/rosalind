#!/usr/bin/python
import urllib

pid = open("rosalind_dbpr.txt").read().strip()

def get_protein_function(pid):
  response = urllib.urlopen("http://www.uniprot.org/uniprot/%s.txt" % pid)
  lines = response.read().split("\n")
  for line in lines:
    if line.startswith("DR"):
      cols = line.split(";")
      if cols[2].strip().startswith("P:"):
        print cols[2].split(":")[1]

get_protein_function(pid)
