#!/usr/bin/python
"""
Pairwise Local Alignment
Problem: Using the water program included in the EMBOSS package and the BLOSUM62
scoring matrix, gap opening penalty of 10, gap extension
penalty of 1.
Given: Two UniProt ID's corresponding to two protein strings s and t
Return: The maximum score of any local alignment of s and t

The following is run as a subprocess:
water -gapopen 10.0 -gapextend 1.0 -outfile rosalind_swat_output.txt
"""
from Bio import SeqIO
import urllib
from subprocess import call

ids = open("rosalind_swat.txt").read().strip().split(" ")

def get_txt_files(ids):
  filenames = []
  for pid in ids:
    filenames.append("%s.txt" % pid)
    h = urllib.urlopen("http://www.uniprot.org/uniprot/%s.txt" % pid)
    outfile = open("%s.txt" % pid, "w")
    outfile.write(h.read())
    outfile.close()
  return filenames

filenames = get_txt_files(ids)
call(["water", "-gapopen", "10.0", "-gapextend", "1.0", "-outfile", "rosalind_swat_output.txt", filenames[0], filenames[1]])
