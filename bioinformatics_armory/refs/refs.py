#!/usr/bin/python
"""
RefSeq Database
Given: A species, two integers a and b, and a date in the form YYYY/MM/DD
Return: The total number of records for genes of length between a and b for the given 
species submitted before the given data. 
"""
from Bio import Entrez

Entrez.email = "mlbernauer@gmail.com"
species, min_length, max_length, date = open("rosalind_refs.txt").read().strip().split("\n")
handle = Entrez.esearch(db="nucleotide", term="%s[ORGN] AND 1:%s[PDAT] %s:%s[SLEN] AND srcdb_refseq[PROP]" % (species, date, min_length, max_length))
print Entrez.read(handle)["Count"]
