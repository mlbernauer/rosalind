#!/usr/bin/python
"""
Introduction to the Bioinformatics Armory
"""
import sys
seq = open(sys.argv[1]).read().strip()
for val in [seq.count(base) for base in "ACGT"]: print val,
