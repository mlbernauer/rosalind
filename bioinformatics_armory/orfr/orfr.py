#!/usr/bin/python
"""
Finding Genes with ORFs
Problem
Given: A DNA string s of length at most 1kbp
Return: The longest protein string that can be translated from an ORF of s.
If more than one protein string of maximum length exists, then you may output any solution.
"""
import subprocess as SP
from Bio import SeqIO
SP.call(["getorf", "-find", "1", "-sequence", "rosalind_orfr.txt", "-outseq", "rosalind_orfr_output.fasta"])
recs = SeqIO.parse(open("rosalind_orfr_output.fasta", "r"), "fasta")
max_len = 0
longest_seq = ""
for rec in recs:
  if len(rec.seq) > max_len:
    longest_seq = rec.seq
    max_len = len(rec.seq)
print longest_seq
