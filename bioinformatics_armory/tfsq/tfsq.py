#!/usr/bin/python
"""
FASTQ Format Introduction
Given: FASTQ file
Return: Corresponding FASTA records
"""
from Bio import SeqIO
infile = open("rosalind_tfsq.txt", "rU")
record = SeqIO.parse(infile, "fastq")
outfile = open("rosalind_tfsq_output.txt", "w")
SeqIO.write(record, outfile, "fasta")
outfile.close()
