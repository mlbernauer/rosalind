#!/usr/bin/python
"""
GenBank Introduction
Given: A genus name, followed by two dates in YYYY/M/D format
Return: The number of Nucleotide GenBank entries for the given genus that were
published between the dates specified. 
"""
from Bio import Entrez

org, start, end = open("rosalind_gbk_input.txt").read().strip().split("\n")
Entrez.email = "mlbernauer@gmail.com"
handle = Entrez.esearch(db="nucleotide", term="%s[Organism] AND %s[PDAT] : %s[PDAT]" % (org, start, end))
record = Entrez.read(handle)
print record["Count"]
