#!/usr/bin/python
"""
Read Filtration by Quality
Problem: Poor qulity reads can be filtered out using the FASTQ Quality Filter from the FASTX toolkit.
Given: A quality threshold value of q, percentage of bases p, and set of FASTQ entries.
Return: Number of reads in filtered FASTQ entries

The following is run as a subprocess:
fastq_quality_filter -q <thresValue> -p <percentBase> -i <inputFile> -Q 33 - o <outFile>
"""
import subprocess as SP

q, p = open("rosalind_filt.txt").readline().strip().split(" ")
lines = open("rosalind_filt.txt").readlines()
file_handle = open("fastq_input.txt",'w')
for line in lines[1:]: file_handle.write(line)
file_handle.close()
outfile = "rosalind_filt_output.txt"
SP.call(["fastq_quality_filter", "-q", q, "-p", p, "-Q", "33", "-v", "-i", "fastq_input.txt", "-o",  outfile])
