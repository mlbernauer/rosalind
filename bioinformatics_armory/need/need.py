#!/usr/bin/python
"""
Pairwise Global Alignment
This python script will read a set of GenBank IDs and 
retrieve their fasta files. 
Pairwise Global Alignment will then be obtained using 
Needle from the EMBOSS package.

Run Needle using the following command
needle -gapopen 10.0 -gapextend 1.0 -endweight -endopen 10.0 -endextend 1.0 -outfile rosalind_need_output.txt <inFile.fasta> <inFile.fasta>
"""
from Bio import Entrez
from Bio import SeqIO

ids = open("rosalind_need.txt").read().strip().replace(" ",", ")
Entrez.email = "mlbernauer@gmail.com"
handle = Entrez.efetch(db="nucleotide", id=[ids], rettype="fasta")
records = list(SeqIO.parse(handle, "fasta"))
for record in records:
  outfile = open("%s.fasta" % record.id.split("|")[-2], "w")
  SeqIO.write(record, outfile, "fasta")
  outfile.close()
  print "Finished writing %s to file!" % record.id.split("|")[-2]
