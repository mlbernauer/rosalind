#!/usr/bin/python
"""
Data Formats
Given: A collection of n (n<10) GenBank entries
Return: The shortest of the strings associeated with the IDs in 
FASTA format.
"""
from Bio import Entrez
from Bio import SeqIO

ids = open("rosalind_frmt.txt").read().strip().replace(" ", ", ")
Entrez.email = "mlbernauer@gmail.com"
handle = Entrez.efetch(db="nucleotide", id=[ids], rettype="fasta")
records = list(SeqIO.parse(handle, "fasta"))
max_length = -1
for record in records:
  if len(record.seq) > max_length:
    max_record = record
output_handle = open("rosalind_frmt_output.fasta","w")
SeqIO.write(max_record, output_handle, "fasta")
output_handle.close()
