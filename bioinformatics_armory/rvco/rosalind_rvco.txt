>Rosalind_3886
GATTTGCACATATTTCTGGCAGATTGCCGTGAATAACCGGCACCCCTAATAACCTTATTC
GGAGGCAGCCGCTTTGAGGTGAAA
>Rosalind_1087
TTACGATACTTATAAAACGCCGTGTGTACGAATGAACCCATCGAGTAACGGTCGCGCGAT
AATTTCCTTTCCCGTCCGTATT
>Rosalind_6409
AGAGGACGCTGAGATATTGTGACAAAGTTCTCCGCGTAGACGTCTACGCGGAGAACTTTG
TCACAATATCTCAGCGTCCTCT
>Rosalind_3711
ACCATATTGGCGGGGTACCTCTGACTCACCACGCTCTAGGCCCTTCAAGCTTACGAATGC
GATTACCTCAACTCCAATTGAAAGCAAGG
>Rosalind_1240
GAATCCCCGAAACTGTTATGTAGATAGCTGCTCGAAGGATCTAAACGTAGGGCCTCCTAG
GCTGTGTATAAAACGTATAATGCTAC
>Rosalind_4829
CAGATGCAGCTCAGGATCTGCCGTAGAGCTTACAGCGAGGCCTCGCTGTAAGCTCTACGG
CAGATCCTGAGCTGCATCTG
>Rosalind_4833
CTTTACTGTTTTGCTTCTAATTGTACCGCCAGCGCCCGCTACTTAATTCAGGACACACAT
ACAGCCTAACGGTGAGGTTA
>Rosalind_2035
CTGCATAAGGCAATTTGGATCCGCGGTTCGCAAGAACGTCATCCAGGCCCTAGTGGTCGT
GCTACTATTGTCATCGAGGCTAGCC
>Rosalind_7292
TCCAGTCGTCGGCCCCCTGAACGTTTTGCACCGCGCCATGCATGGCGCGGTGCAAAACGT
TCAGGGGGCCGACGACTGGA
>Rosalind_1790
GAGGACGCCTAATAGAGTCCCTGCAATCAAAGAAAACCCTTGCAAGGGTTTTCTTTGATT
GCAGGGACTCTATTAGGCGTCCTC
