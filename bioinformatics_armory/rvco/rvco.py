#!/usr/bin/python
"""
Complementing a Strand of DNA
Problem: 
Given: A collection of n (n <= 10) DNA strings.
Return: The number of given strings that match their reverse complements.
"""
from Bio import Seq
from Bio import SeqIO
from Bio.Alphabet import IUPAC
records = SeqIO.parse(open("rosalind_rvco.txt","r"), "fasta")
count = 0
for rec in records:
  if rec.seq.tostring() == rec.seq.reverse_complement().tostring():
    count += 1
print count

