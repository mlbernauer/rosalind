#!/usr/bin/python
"""
Problem: The novel-motif finding tool MEME can be found in the link below.
Given: A set of protein strings in FASTA format that share some motif with minimu length 20.
Return: A regular expression for the best-scoring motif. 
There is not source code for this exercies.
The complete this exercise, upload the sequence fasta
to http://meme.nbcr.net/meme/cgi-bin/meme.cgi 
The resulting regex can be seen in rosalind_meme_output.txt
"""
