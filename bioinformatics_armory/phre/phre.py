#!/usr/bin/python
"""
Read Quality Distribution
"""
from Bio import SeqIO
import numpy as np

def get_threshold(infile):
  f = open(infile)
  file_name = "rosalind_phre_fastq.txt"
  outfile = open(file_name,"w")
  count = 0
  for line in f:
    count += 1
    if count == 1:
      threshold = line.strip()
      continue
    outfile.write(line)
  outfile.close()
  return threshold, file_name

def find_poor_scores(infile, thresh):
  records = SeqIO.parse(open(infile, "rU"), "fastq")
  count = 0
  for rec in records:
    if np.average(rec.letter_annotations["phred_quality"]) <= thresh:
      count += 1
  print count
  
thresh, filename = get_threshold("rosalind_phre.txt")
thresh = int(thresh)
find_poor_scores(filename, thresh)
