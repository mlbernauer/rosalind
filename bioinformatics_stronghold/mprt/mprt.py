#!/usr/bin/python
"""
Finding A protein Motif
"""
import sys
import urllib
import re

#id_file = sys.argv[1]
def get_sequences(id_file):
  prot_ids = open(id_file).read().strip().split("\n")
  base_url = "http://www.uniprot.org/uniprot/%s.fasta"
  for pid in prot_ids:
    f = urllib.urlopen(base_url % pid)
    lines = f.read().strip().split("\n")
    seq = ""
    for line in lines:
      if line[0] == ">":
        header = pid
        continue
      seq += line
    yield (header,seq)

def find_motif(id_file):
  for pid, seq in get_sequences(id_file):
    pos = [m.start()+1 for m in re.finditer(r"(?=(N[^P][ST][^P]))", seq)]
    if len(pos) > 0:
      print pid
      for p in pos: print p,
      print

find_motif(sys.argv[1])
     
