#!/usr/bin/python
"""
Enumerating Gene Orders
"""
import sys
import itertools
n = int(open(sys.argv[1]).read().strip())
permutations = [i for i in itertools.permutations([i+1 for i in range(n)])]
print len(permutations)
for i in permutations:
  for j in i:
    print j,
  print
