#!/usr/bin/python
"""
Locating Restriction Sites
"""
import sys
import re

def get_sequence(filename):
  lines = open(sys.argv[1]).read().strip().split("\n")
  sequence = {}
  for line in lines:
    if line.startswith(">"):
      header = line[1:]
      sequence.setdefault(header,"")
      continue
    sequence[header] += line
  return sequence[header]

def get_rev_complement(seq):
  return seq.replace("A","t").replace("C","g").replace("G","c").replace("T","a").upper()[::-1]

def find_palindromes(seq):
  palindromes = set()

  for k in range(4,13):
    kmers = [m.groups()[0] for m in re.finditer("(?=(\w{%d}))" % k, seq)]
    for kmer in kmers:
      if kmer == get_rev_complement(kmer):
        pos = [m.start() for m in re.finditer("(?=(%s))" % kmer, seq)]
        for p in pos:
          palindromes.add((str(p+1), str(len(kmer))))
  return [list(i) for i in palindromes]

seq = get_sequence(sys.argv[1])
for i in find_palindromes(seq): print " ".join(i)  
