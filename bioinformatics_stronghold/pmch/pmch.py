#!/usr/bin/python
"""
Perfect Matchings and RNA Secondary Structures
"""
import sys
n = int(sys.argv[1])
def get_matches(n):
  if n == 1: return 1
  else: return get_matches(n-1)*(2*n-1)

print get_matches(n)
