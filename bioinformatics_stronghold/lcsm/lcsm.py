#!/usr/bin/python
"""
Finding a Shared Motif
Given: A collection of k (k <= 100) DNA strings of length at most 1kbp each in FASTA
format
Return: A longest common substring of the collection. (If multiple solutions exist, you
may return any single solution).
"""
import re

def get_data():
  f = open("rosalind_lcsm.txt", "rb")
  data = {}
  seq_id = ""
  for line in f:
    if line[0] == ">":
	  seq_id = line.strip()[1:]
	  continue
    data.setdefault(seq_id,"")
    data[seq_id] += line.strip()
  return data

def get_kmers(string, k):
  return [m.groups()[0] for m in re.finditer(r"(?=(\w{%d}))" % k, string)]

def inseq(kmer, data):
  result = True
  for seq_id in data:
	if kmer not in data[seq_id]:
	  result = False
  return result

def find_longest_shared_motif():
  data = get_data()
  kmer_length = 1
  common_motifs = []
  max_motif_length = -1
  first_seq = data[data.keys()[0]]
  while kmer_length < len(first_seq):
	kmers = get_kmers(first_seq, kmer_length)
	for kmer in kmers:
	  if inseq(kmer, data) and len(kmer) > max_motif_length:
		max_motif_length = len(kmer)
		common_motifs = [kmer]
	  if inseq(kmer, data) and len(kmer) == max_motif_length and kmer not in common_motifs:
		common_motifs.append(kmer)
	kmer_length += 1
  return common_motifs

print find_longest_shared_motif()[0]

		
