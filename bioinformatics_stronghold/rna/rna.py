#!/usr/bin/python
"""
Transcribing DNA into RNA
Given: A DNA string t having length at most 1000 nt.
Return: The transcribed RNA string of t.
"""
seq = open("rosalind_rna.txt").read().strip()
out_seq = ""
for i in seq: 
  if i == "T":
	out_seq += "U"
  else:
	out_seq += i
print out_seq
