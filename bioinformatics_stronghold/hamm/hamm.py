#!/usr/bin/python
"""
Counting Point Mutations
Given: Two DNA strings s and t of equal length (not exceeding 1kbp).
Return: The hamming distance d(s,t)
"""
seq1, seq2 = open("rosalind_hamm.txt").read().strip().split("\n")
dist = 0
for i in range(len(seq1)):
  if seq1[i] != seq2[i]: dist += 1

print dist
