#!/usr/bin/python
"""
Computing GC Content
Given: At most 10 DNA strings in FASTA format (of length at most 1kbp each).
Return: The ID of the string having the highest GC-content, followed by the GC-content of that string.
Rosalind allows for a default error of 0.001 in all decimal answers unless otherwise stated;
please see the note on absolute error below. 
"""
f = open("rosalind_gc.txt", "rb")
data = {}
seq_id = ""
for line in f:
  if line[0] == ">":
	seq_id = line.strip()[1:]

	continue
  data.setdefault(seq_id,"")
  data[seq_id] += line.strip()

gc = [((data[i].count("G") + data[i].count("C"))/float(len(data[i])), i) for i in data]

print max(gc)[1]
print max(gc)[0]*100
