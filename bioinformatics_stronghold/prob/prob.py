#!/usr/bin/python
"""
Introduction to Random Strings
Modeling Random Genomes
Problem:
Given: A DNA string s of length at most 100bp and an array A containing
at most 20 numbers between 0 and 1
Return: An array B having the same length as A in which B[k] represents 
the common logarithm of the probability that a random string constructed
with the GC-content found in A[k] will match s exactly.
"""
import sys
import math

seq, gc = open(sys.argv[1]).read().strip().split("\n")
gc = [float(i) for i in  gc.split(" ")]

def probs(seq, gc):
  log_probs = []
  for gc_prob in gc:
    probs = {"G":gc_prob/2.0, "C":gc_prob/2.0, "A":(1-gc_prob)/2.0, "T":(1-gc_prob)/2.0}
    match_prob = 1
    for base in seq:
      match_prob *= probs[base]
    log_probs.append(math.log10(match_prob))
  return log_probs

for i in probs(seq, gc): print i,
