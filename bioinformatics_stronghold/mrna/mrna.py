#!/usr/bin/python
"""
Inferring mRNA from Protein
"""
import sys
import re

seq = open(sys.argv[1]).read().strip()
code = """UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G"""

def get_aa_counts(genetic_code):
  code = [(i.split(" ")[1],i.split(" ")[0]) for i in re.split(" {2,}", genetic_code.replace("\n","  "))]
  aa_counts = {}
  for aa, rna in code:
    aa_counts.setdefault(aa,0)
    aa_counts[aa] += 1
  return aa_counts

def get_modulo(seq, genetic_code, modulo):
  aa_counts = get_aa_counts(genetic_code)
  rna_counts = 1
  for aa in seq:
    rna_counts *= aa_counts[aa]
  rna_counts *= aa_counts["Stop"]
  return rna_counts % modulo

print get_modulo(seq, code, 1000000)
