#!/usr/bin/python
"""
Rabbits and Recurrence Relations
Given: Positive integers n <= 40 and k <= 5.
Return: The total number of rabbit pairs that will be present after n months if we begin with 1 pair and in
each generation, every pari of reproduction-age rabbits produces a litter of k rabbit pairs (instead of only
1 pair).
"""
N, m = open("rosalind_fib.txt").read().strip().split(" ")
N = int(N)
m = int(m)
def f(n):
  if n == 0: return 0
  elif n == 1: return 1
  else: return f(n-1) + m*f(n-2)

o = f(N)
print o
