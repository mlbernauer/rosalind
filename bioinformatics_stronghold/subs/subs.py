#!/usr/bin/python
"""
Finding a Motif in DNA
Given: Two DNA strings s and t (each of length at most 1kbp).
Return: All locations of t as a substring of s.
"""
import re
seq, sub = open("rosalind_subs.txt").read().strip().split("\n")
pos = [m.start() for m in re.finditer(r"(?=(%s))" % sub, seq)]
for i in pos: print i+1,
