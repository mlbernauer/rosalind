#!/usr/bin/python
"""
Partrial Permutations
Given: Positive integers n and k such that 100 >= n and 10 >= k > 0
Return: The total number of partial permutations P(n,k), modulao 1,000,000.
"""
import itertools
n, k = open("rosalind_pper.txt").read().strip().split(" ")
n = int(n)
k = int(k)
def perms(n,k):
  count = 0
  for i in itertools.permutations(range(n), k): count += 1
  return count % 1000000

print perms(n,k)  
