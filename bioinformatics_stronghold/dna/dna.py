#!/usr/bin/python
"""
Counting DNA Nucleotides
Given: A DNA string s of length at most 1000 nt.
Return: Four integers (separated by spaces) counting the respective number of times that the symbols
"A", "C", "G", "T" occur in s.
"""
seq = open("rosalind_dna.txt", "rb").read().strip()
bases = "ACGT"
for base in bases: print seq.count(base),
