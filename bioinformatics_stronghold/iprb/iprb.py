#!/usr/bin/python
"""
Mendel's First Law
Given: Three positive integers k, m, and n, representing a population containing k + m + n organisms:
k individuals are homozygous dominant for a factor, m are heterozygous, and n are homozygous recessive. 
Return: The probablity that two randomly selected mating organisms will produce an individual possessing
a dominant allele (and thus displaying the dominant phenotype). Assume that any two organisms can mate. 
"""
k, m, n = open("rosalind_iprb.txt").read().strip().split(" ")
k = int(k)
m = int(m)
n = int(n)

t = k + m +n
out_k = (k/float(t))*(k-1+m+n)
out_m = (m/float(t))*(k + 0.75*m - .75 + 0.5*n)
out_n = (n/float(t))*(k + 0.5*m)

print (1/float(t-1))*(out_k + out_m + out_n)

