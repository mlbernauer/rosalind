#!/usr/bin/python
"""
Translating RNA into Protein
Given: An RNA string s corresponding to a strand of mRNA (of length at most 10kbp).
Return: The protein string encoded by s.
"""
import re
seq = open("rosalind_prot.txt", "rb").read().strip()

data = """UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G"""

gen_code = dict([(i.split(" ")[0], i.split(" ")[1]) for i in re.split(" {2,}", data.replace("\n", "  "))])
print "".join([gen_code.get(i,"") for i in re.split("(\w{3})", seq) if gen_code.get(i) != "Stop"])
