#!/usr/bin/python
"""
Transitions and Transversions
"""
import sys

def get_sequences(filename):
  sequences = {}
  lines = open(filename).read().strip().split("\n")
  for line in lines:
    if line.startswith(">"):
      header = line[1:]
      sequences.setdefault(header,"")
      continue
    sequences[header] += line
  return sequences.values()

def mutation_type(base1, base2):
  base_type = {"A":"purine", "G":"purine", "C":"pyrimidine", "T":"pyrimidine"}
  return "Transversion" if base_type[base1] != base_type[base2] else "Transition"

def get_ratio(seq1, seq2):
  mutation_counts = {}
  for pos in range(len(seq1)):
    if seq1[pos] != seq2[pos]:
      mutation_counts.setdefault(mutation_type(seq1[pos], seq2[pos]),0)
      mutation_counts[mutation_type(seq1[pos], seq2[pos])] += 1
  return mutation_counts["Transition"] / float(mutation_counts["Transversion"])

seq1, seq2 = get_sequences(sys.argv[1])
print get_ratio(seq1, seq2)
