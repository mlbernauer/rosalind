#!/usr/bin/python
"""
Open Reading Frames
"""
import sys
import re

def get_seq(filename):
  lines = open(filename).read().strip().split("\n")
  header = ""
  seq = ""
  for line in lines:
    if line[0] == ">":
      header = line
      continue
    seq += line
  return (header, seq)

def translate_codon(codon):
  code = """UUU F      CUU L      AUU I      GUU V
  UUC F      CUC L      AUC I      GUC V
  UUA L      CUA L      AUA I      GUA V
  UUG L      CUG L      AUG M      GUG V
  UCU S      CCU P      ACU T      GCU A
  UCC S      CCC P      ACC T      GCC A
  UCA S      CCA P      ACA T      GCA A
  UCG S      CCG P      ACG T      GCG A
  UAU Y      CAU H      AAU N      GAU D
  UAC Y      CAC H      AAC N      GAC D
  UAA Stop   CAA Q      AAA K      GAA E
  UAG Stop   CAG Q      AAG K      GAG E
  UGU C      CGU R      AGU S      GGU G
  UGC C      CGC R      AGC S      GGC G
  UGA Stop   CGA R      AGA R      GGA G
  UGG W      CGG R      AGG R      GGG G"""
  rna_code = dict([(i.split(" ")[0], i.split(" ")[1]) for i in re.split(" {2,}", code.replace("\n", "  "))])
  return rna_code.get(codon,"-")

def translate_sequence(seq):
  protein = ""
  for i in [i for i in range(len(seq)-3) if i%3 == 0]:
    codon = seq[i:i+3]
    if translate_codon(codon) == "Stop": 
      return protein
    protein += translate_codon(codon)

def dna2rna(dna):
  return dna.replace("T","U")

def get_reverse_comp(rna):
  return seq.replace("C","g").replace("G","c").replace("T","a").replace("A","U").upper()[::-1]
  
def translate_protein(dna):
  proteins = set()
  rna = dna2rna(dna)
  rev = get_reverse_comp(rna)
  for i in range(3):
    orf = rna[i:]
    rorf = rev[i:]
    for j in [k for k in range(len(orf)-3) if k%3==0]:
      codon = orf[j:j+3]
      rcodon = rorf[j:j+3]
      if translate_codon(codon) == "M":
        proteins.add(translate_sequence(orf[j:]))
      if translate_codon(rcodon) == "M":
        proteins.add(translate_sequence(rorf[j:]))
  if None in proteins: proteins.remove(None)
  return proteins

header, seq = get_seq(sys.argv[1])
for i in translate_protein(seq): print i
