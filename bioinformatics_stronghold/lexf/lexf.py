#!/usr/bin/python
"""
Enumerating k-mers Lexicographically
"""
import sys
import itertools

bases, n = open(sys.argv[1]).read().strip().split("\n")
bases = bases.split(" ")
n = int(n)

for comb in itertools.product(bases,repeat=n): print "".join(comb)
