#!/usr/bin/python
"""
Complementing a Strand of DNA
Given: A DNA string s of length at most 1000 bp.
Return: The reverse complement of s.
"""
seq = open("rosalind_revc.txt").read().strip()
out_seq = ""
comp = {"G":"C", "C":"G", "A":"T", "T":"A"}
for i in seq:
  out_seq += comp[i]

print out_seq[::-1]
