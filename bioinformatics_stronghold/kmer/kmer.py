#!/usr/bin/python
"""
k-Mer Composition
Problem: 
GIVEN: a DNA string of S in FASTA format (having length at most 100kbp)
RETURN: the 4-mer composition of s
"""
import sys
import itertools
import re

def get_seq(filename):
  seq = ""
  lines = open(filename).read().strip().split("\n")
  for line in lines:
    if line.startswith(">"):
      continue
    seq += line
  return seq

def get_kmer_comp(seq, k):
  kmer_counts = {}
  kmers = ["".join(i) for i in itertools.product("GCAT", repeat=k)]
  for kmer in kmers:
    kmer_counts[kmer] = len(re.findall("(?=%s)" % kmer, seq))
  ordered_kmers = sorted(kmer_counts.items())
  return [i[1] for i in ordered_kmers]

seq = get_seq(sys.argv[1])
for i in get_kmer_comp(seq,4): print i,
