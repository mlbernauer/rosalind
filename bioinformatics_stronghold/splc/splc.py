#!/usr/bin/python
"""
RNA Splicing
"""
import sys
import re

def get_sequences(filename):
  sequences = {}
  lines = open(filename).read().strip().split("\n")
  count = 0
  for line in lines:
    count += 1
    if count == 1:
      gene_header = line[1:]
    if line.startswith(">"):
      header = line[1:]
      sequences.setdefault(header,"")
      continue
    sequences[header] += line
  gene = sequences[gene_header]
  introns = [i for i in sequences.values() if i != gene]
  return gene, introns

def dna2rna(dna):
  return dna.replace("T","U")

def translate(rna):
  code = """UUU F      CUU L      AUU I      GUU V
  UUC F      CUC L      AUC I      GUC V
  UUA L      CUA L      AUA I      GUA V
  UUG L      CUG L      AUG M      GUG V
  UCU S      CCU P      ACU T      GCU A
  UCC S      CCC P      ACC T      GCC A
  UCA S      CCA P      ACA T      GCA A
  UCG S      CCG P      ACG T      GCG A
  UAU Y      CAU H      AAU N      GAU D
  UAC Y      CAC H      AAC N      GAC D
  UAA Stop   CAA Q      AAA K      GAA E
  UAG Stop   CAG Q      AAG K      GAG E
  UGU C      CGU R      AGU S      GGU G
  UGC C      CGC R      AGC S      GGC G
  UGA Stop   CGA R      AGA R      GGA G
  UGG W      CGG R      AGG R      GGG G"""
  rnadict = dict([(i.split(" ")[0], i.split(" ")[1]) for i in re.split(" {2,}", code.replace("\n","  "))])
  protein = ""
  for i in [j for j in range(len(rna)-3) if j%3 == 0]:
    codon = rna[i:i+3]
    if rnadict[codon] == "Stop":
      return protein
    else: protein += rnadict.get(codon,"-")
  return protein

def remove_introns(seq, introns):
  for i in introns:
    if seq.count(i) == 1:
      idx = seq.index(i)
      spliced = seq[idx:idx+len(i)]
      seq = seq[:idx] + seq[idx+len(i):]
    elif seq.count(i) > 1:
      while seq.count(i) > 0:
        idx = seq.index(i)
        spliced = seq[idx:idx+len(idx)]
        seq = seq[:idx] + seq[idx+len(i):]
    else: continue
  return seq

gene, introns = get_sequences(sys.argv[1])
spliced_gene = remove_introns(gene, introns)
translated_gene = dna2rna(spliced_gene)
print translate(translated_gene)

