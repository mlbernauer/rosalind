#!/usr/bin/python

# Given: A DNA string s of length at most 1000 nt
# Return: Four integers (separated by spaces) counting
# the respective number of times that the symbols 'A', 'C', "G', 'T'
# occurs in S
import sys
print " ".join([str(open(sys.argv[1], 'r').read().count(i)) for i in ['A', 'C', 'G', 'T']])

