#!/usr/bin/python

# Given: Two DNA strings 's' and 't' of equal length (not exceeding 1 kbp)
# Return: The hamming distance dH(s,t)

import sys
s1,s2 = open(sys.argv[1], 'r').read().strip().split("\n")
print sum([1 for i in range(len(s1)) if s1[i] != s2[i]])
