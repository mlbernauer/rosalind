#!/usr/bin/python
# Given: A DNA string 't' having length at most 1000 nt
# Return: the transcribed RNA string of 't'
import sys
print open(sys.argv[1], 'r').read().replace("T", "U")
