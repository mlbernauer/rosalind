#!/usr/bin/python

# Given: At most 10 DNA strings in FASTA format (of length at most 1 kbp each).
# Return: Th ID of the strings having highest GC-content, folowwed by the GC-content of that string.
# Rosalind allows for a default error of 0.001 in all decimal answers unless otherwise stated; please
# see the note on absolute error below.

import sys

f = [i for i in open(sys.argv[1], 'r').read().strip().split("\n") if i != ""]
seq = {}
seqid = ""
for i in f:
    if i[0] == ">":
        seqid = i[1:]
        seq.setdefault(seqid,"")
        continue
    seq[seqid] += i


gc = [(seq[i].count("G") + seq[i].count("C"))/float(len(seq[i])) for i in seq.keys()]
idx = gc.index(max(gc))

print seq.keys()[idx]
print gc[idx]*100
