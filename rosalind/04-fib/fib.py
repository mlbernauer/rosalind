#!/usr/bin/python

# Given: Positive integers n <= 40 and k <= 5
# Return: The total number of rabbits pairs that will be present
# after n months if we be begin with 1 pair and in each generation,
# every pair of reproduction-age rabbits produces a litter of 'k' rabbit pairs
# instead of only 1 pair

import sys

def fib(n, k=1):
    current_gen, prev_gen = 1,0
    for i in range(n):
        current_gen, prev_gen = current_gen + k*prev_gen, current_gen
    return prev_gen

n,k = [int(i) for i in open(sys.argv[1], 'r').read().split(" ")]

print fib(n, k=k)
