#!/usr/bin/python

# Given: A DNA string 's' of length at most 1000 bp
# Return: The reverse compliement 'sc' of s

import sys
print "".join([{"G":"C", "C":"G", "A":"T", "T":"A"}.get(i,"") for i in open(sys.argv[1], 'r').read()])[::-1]
