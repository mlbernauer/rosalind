#!/usr/bin/python
"""
Reverse Complement Problem
"""
import sys
seq = open(sys.argv[1]).read().strip()
comp = {"A": "T", "T":"A", "G":"C", "C":"G"}
print "".join([comp[i] for i in seq])[::-1]
