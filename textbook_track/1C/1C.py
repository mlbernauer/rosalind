#!/usr/bin/python
"""
Pattern Matching Problem
"""
pattern, seq = open(sys.argv[1]).read().strip().split("\n")
k = len(pattern)
locations = [i for i in range(len(seq)-k) if seq[i:i+k] == pattern]
for loc in locations: print loc,
