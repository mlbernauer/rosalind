#!/usr/bin/python
"""
Frequent Words with Mismatch Problem
"""
import sys
from collections import Counter

seq, data = open(sys.argv[1]).read().strip().split("\n")
k = int(data.split(" ")[0])
d = int(data.split(" ")[1])
#seq, k, d = open(sys.argv[1]).read().strip().split(" ")
#k = int(k)
#d = int(d)

def match(pat1, pat2, d):
  mismatch = 0
  for i in range(len(pat1)):
    if pat1[i] != pat2[i]: mismatch+=1
  return True if mismatch <= d else False

def mutate(kmers):
  bases = ["G", "C", "A", "T"]
  mutants = []
  for kmer in kmers:
    mutants.append(kmer)
    for p in range(len(kmer)):
      for b in bases: 
        pat = list(kmer)
        pat[p] = b
        newp = "".join(pat)
        mutants.append(newp)
  return mutants

def generate_mutants(kmers, d):
  mutants = kmers
  while d > 0:
    d -= 1
    mutants = [i for i in mutate(mutants) if i not in kmers]
  return list(set(mutants ))

def find_frequent_mismatch(seq, k, d):
  kmers = list([seq[i:i+k] for i in range(len(seq)-k+1 )])
  counts = {}
  for kmer in kmers:
    for mut in generate_mutants([kmer],d):
      counts.setdefault(mut,0)
      counts[mut] += 1
  sorted_counts = sorted([(i[1],i[0]) for i in counts.iteritems()])
  max_counts = [i[1] for i in sorted_counts if i[0] == sorted_counts[-1][0]]
  return max_counts
for i in find_frequent_mismatch(seq, k, d): print i,

