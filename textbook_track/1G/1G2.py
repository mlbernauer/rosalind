#!/usr/bin/python
"""
Frequent Words with Mismatch Problem
"""
import sys

seq, data = open(sys.argv[1]).read().strip().split("\n")
k = int(data.split(" ")[0])
d = int(data.split(" ")[1])

def match(pat1, pat2, d):
  mismatch = 0
  for i in range(len(pat1)):
    if pat1[i] != pat2[i]: mismatch+=1
  return True if mismatch <= d else False

def mutate(kmers):
  bases = ["G", "C", "A", "T"]
  mutants = []
  for kmer in kmers:
    for p in range(len(kmer)):
      for b in bases:
        pat = list(kmer)
        pat[p] = b
        newp = "".join(pat)
        if newp not in kmers: mutants.append(newp)
  return mutants

def generate_mutants(kmers, d):
  mutants = kmers
  while d > 0:
    d -= 1
    mutants = mutate(mutants)
  return mutants 

def find_frequent_mismatch(seq, k, d):
  kmers = list(set([seq[i:i+k] for i in range(len(seq)-k+1)]))
  mutants = list(set(generate_mutants(kmers,d)))
  print "%d mutants found!" % len(mutants)
  # Score survivors
  counts = {}
  for i in range(len(seq)-k+1):
    wind = seq[i:i+k]
    for m in mutants:
      if match(m,wind,d):
        counts.setdefault(m,0)
        counts[m] += 1
  sorted_counts = sorted([(i[1],i[0]) for i in counts.iteritems()])
  max_counts = [i[1] for i in sorted_counts if i[0] == sorted_counts[-1][0]]
  return max_counts

for i in find_frequent_mismatch(seq, k, d): print i,

