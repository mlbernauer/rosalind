#!/usr/bin/python
"""
Clump Finding Problem
"""
import sys
seq, data = open(sys.argv[1]).read().strip().split("\n")
k, L, t = [int(i) for i in data.split(" ")]

def find_clumps(seq, k, l, t):
  kmers = {}
  clusters = {}
  for i in range(len(seq)+1-k):
    kmer = seq[i:i+k]
    kmers.setdefault(kmer,[])
    kmers[kmer].append(i)
  for kmer in kmers:
    positions = sorted(kmers[kmer])
    if len(positions) == t and (max(positions)+ k - min(positions)) < l:
      clusters[kmer] = (kmers[kmer], max(positions)-min(positions))
    elif len(positions) > t:
      for j in range(len(positions)+1-t):
        pos_range = ((positions[j+t-1] + k) - positions[j])
        if pos_range <= l:
          clusters[kmer] = (positions[j:j+t], pos_range)
    else:
      continue
  return clusters

print len(find_clumps(seq, k, L, t))
      
