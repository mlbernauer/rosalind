#!/usr/bin/python
"""
Minimum Skew Problem
"""
import sys
seq = open(sys.argv[1]).read().strip()
def skew(seq):
  skew = 0
  score = {"C":-1, "G":1}
  vals = [skew]
  for nuc in seq:
    skew += score.get(nuc,0)
    vals.append(skew)
  return vals

def minimize_skew(seq):
  skews = skew(seq)
  sorted_skews = sorted(range(len(skews)), key=lambda x: skews[x])
  return [i for i in sorted_skews if skews[i] == skews[sorted_skews[0]]]

#for i in skew(seq): print i,
for i in minimize_skew(seq): print i,
