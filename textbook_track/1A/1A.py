#!/usr/bin/python
"""
Frequent Words Problem
"""
import sys, re
from collections import Counter
seq, k = open(sys.argv[1]).read().strip().split("\n")
k = int(k)
kmers = [m.groups()[0] for m in re.finditer(r"(?=(\w{%d}))" % k, seq)]
kmer_counts = Counter(kmers)
max_count = kmer_counts.most_common(1)[0][1]
for i in kmer_counts.most_common():
  if i[1] == max_count:
    print i[0],













