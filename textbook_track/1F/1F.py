#!/usr/bin/python
"""
Approximate Matching Problem
"""
import sys
pattern, seq, d = open(sys.argv[1]).read().strip().split("\n")
d = int(d)

def is_match(pat1, pat2, d):
  mismatch = 0
  for i in range(len(pat1)):
    if pat1[i] != pat2[i]: mismatch += 1
  if mismatch <= d:
    return True
  else:
    return False

def frequent_mismatch(seq, pattern, d):
  k = len(pattern)
  positions = []
  for i in range(len(seq)-k+1):
    pat = seq[i:i+k]
    if is_match(pat,pattern,d): positions.append(i)
  return positions

def count(seq, pat, d):
  k = len(pat)
  count = 0
  for i in range(len(seq)-k+1):
    pat2 = seq[i:i+k]
    if is_match(pat,pat2,d): count+=1
  return count

#matches = frequent_mismatch(seq, pattern,d)
#for position in matches: print position,
print count(seq, pattern, d)
